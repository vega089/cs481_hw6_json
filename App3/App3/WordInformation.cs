﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Globalization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace App3
{   
    public partial class WordInformation
    {
        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("definition")]
        public string Definition { get; set; }

        [JsonProperty("example")]
        public string Example { get; set; }
    }

    public partial class WordInformation
    {
        public static WordInformation[] FromJson(string json) => JsonConvert.DeserializeObject<WordInformation[]>(json, App3.Converter.Settings);
    }
    public static class Serialize
    {
        public static string ToJson(this WordInformation[] self) => JsonConvert.SerializeObject(self, App3.Converter.Settings);
    }
    internal static class Converter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters =
            {
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
        };
    }
}