﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using Xamarin.Forms;
using Xamarin.Essentials;

namespace App3
{
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {

        public string InputFromUser { get; set; }//get set for string from user
        public MainPage()
        {
            BindingContext = this;
            InitializeComponent();
        }

        async void Button_Clicked(object sender, EventArgs e)
        {
            HttpClient Client = new HttpClient(); //creating new client

           //use owlbot url concatonated with input followwed by the json format
            Uri uri = new Uri("https://owlbot.info/api/v2/dictionary/" + InputFromUser + "?format=json");
            HttpRequestMessage request = new HttpRequestMessage();
            request.Method = HttpMethod.Get;
            request.RequestUri = uri; //pass uri to request


            HttpResponseMessage response = null;

            try
            {
                response = await Client.SendAsync(request);//reponse from cluent 
            }
            catch (Exception exception)
            {
                //resonse failed
                await DisplayAlert("Failure", "check internet status ", "Ok");
                return;
            }
                WordInformation[] Word = null; //word is a type of WordInformation

            if (response.IsSuccessStatusCode) //uses status code as a check for successful word data retrieval 
            {
                string json = await response.Content.ReadAsStringAsync(); //converts to string 

                try
                {
                    Word = WordInformation.FromJson(json); //uses json to give the word/input its data
                }
                catch (Exception exception)
                {
                    
                    await DisplayAlert("Failure", "Failed to retrieve word data , check spelling", "Ok"); //if there is an issue with json/word
                    return; 
                }
             
                string WordType = ""; //type
                string WordDefinition = ""; //definition
                string ExampleOfWord = ""; //example

                if (Word[0].Type != null)
                {
                    WordType = Word[0].Type; //set type = to the json type value
                    this.type.Text = WordType;//set text = to the word type 
                }
                else
                {
                    this.type.Text = "No Type Available";//if null make text equal to not available 
                }

                if (Word[0].Definition != null) //check for null
                {
                    WordDefinition = Word[0].Definition;//set definition string to word.example value 
                    this.definition.Text = WordDefinition;//set text to definition string value 
                }
                else
                {
                    this.definition.Text = "No Definition Available";
                }


                if (Word[0].Example != null) //check for null
                {
                    ExampleOfWord = Word[0].Example; //set example string to word.example value 
                    this.example.Text = ExampleOfWord; //set text to example string value 
                }
                else
                {
                    this.example.Text = "No Example Available"; //if null make text equal to not available 

                }  
            }
        }

        void Intenet_State(object sender, EventArgs e) //buton for checking internet 
        {
            var internetState = Connectivity.NetworkAccess; //using essentials
            if (internetState == NetworkAccess.Internet) //if online
                DisplayAlert("Success", "Internet access : ONLINE ", "Ok"); //display alert
            else //if NOT online 
            { 
            DisplayAlert("Failure", "Internet access : OFFLINE ", "Ok");
            }
        }
    }
}
